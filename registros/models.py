from django.db import models
from django.urls import reverse

class Registro(models.Model):
    STATUS = [
        ('RT-PCR', 'RT-PCR'),
        ('Sorologia', 'Sorologia'),
        ('Teste Rápido - Antígenos', 'Teste Rápido - Antígenos'),
        ('Teste Rápido - Anticorpos', 'Teste Rápido - Anticorpos')
    ]
    nome_completo = models.CharField(max_length=60)
    data_nascimento = models.DateField()
    tipo_teste = models.CharField(
       max_length=32,
       choices=STATUS,
   )
    resultado_teste = models.BooleanField(default=False)
