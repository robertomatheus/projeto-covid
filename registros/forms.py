from django import forms
from .models import Registro

class RegistroForm(forms.ModelForm):
    nome_completo = forms.CharField(label='Nome Completo', widget=forms.TextInput(attrs={"placeholder": "Nome completo"}))
    data_nascimento = forms.DateField(
        label='Data de Nascimento:', 
        widget= forms.DateInput(format='%m/%d/%Y'),
        input_formats=('%m/%d/%Y', )
    )
    
    class Meta:
        model = Registro
        fields = '__all__'
        widgets = {
            'nome_completo': forms.TextInput(attrs={'class': 'form-control'}),
        }