from django.shortcuts import render, get_object_or_404, redirect
from .forms import RegistroForm
from .models import Registro


def registro_create_view(request):
    form = RegistroForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = RegistroForm()
    context = {
        'form': form
    }
    return render(request, "registros/criar_registro.html", context)


def registro_update_view(request, id=id):
    obj = get_object_or_404(Registro, id=id)
    form = RegistroForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, "registros/criar_registro.html", context)


def registro_list_view(request):
    queryset = Registro.objects.all() # list of objects
    context = {
        "object_list": queryset
    }
    return render(request, "registros/listar_registro.html", context)

def registro_delete_view(request, id):
    obj = get_object_or_404(Registro, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect('../../')
    context = {
        "object": obj
    }
    return render(request, "registros/excluir_registro.html", context)