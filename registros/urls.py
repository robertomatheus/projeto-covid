from django.urls import path
from .views import (
    registro_create_view, 
    registro_delete_view,
    registro_list_view,
    registro_update_view,
)

app_name = 'registro'
urlpatterns = [
    path('', registro_list_view, name='registro-list'),
    path('criar/', registro_create_view, name='registro-list'),
    path('<int:id>/editar/', registro_update_view, name='registro-update'),
    path('<int:id>/remover/', registro_delete_view, name='registro-delete'),
]