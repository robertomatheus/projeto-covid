# Generated by Django 3.0.8 on 2020-07-23 17:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Registro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome_completo', models.CharField(max_length=60)),
                ('data_nascimento', models.DateField()),
                ('tipo_teste', models.CharField(choices=[('RT-PCR', 'RT-PCR'), ('Sorologia', 'Sorologia'), ('Teste Rápido - Antígenos', 'Teste Rápido - Antígenos'), ('Teste Rápido - Anticorpos', 'Teste Rápido - Anticorpos')], max_length=32)),
                ('resultado_teste', models.BooleanField(default=False)),
            ],
        ),
    ]
