from rest_framework import viewsets

from .serializers import RegistroSerializer
from registros.models import Registro


class RegistroViewSet(viewsets.ModelViewSet):
    queryset = Registro.objects.all().order_by('nome_completo')
    serializer_class = RegistroSerializer